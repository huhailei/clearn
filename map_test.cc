#if 0
g++ ./map_test.cc -o map_test && ./map_test
exit $?
#endif

#include <iostream>
#include <map>

int main() {
	std::map<int, std::string *> m_data;
	std::cout << "---------" << std::endl;
	std::string s("dudu");
	std::cout << "*******" << std::endl;
	std::cout << "Original: " << s << std::endl;
	m_data.insert(std::pair<int, std::string *>(22, &s));
	std::map<int, std::string *>::iterator i ;
	i = m_data.find(22);
	std::string *j;
	j = i->second;
	std::cout << *j << std::endl;
	j = m_data[22];
	std::cout << *j << std::endl;

	*j += "orange";
	std::cout << *j << std::endl;
	m_data[22] = j;
	std::cout << *(m_data[22]) << std::endl;

	j = m_data[23];
	std::cout << j << std::endl; 
	if (j == 0) {
		std::cout << j << std::endl;
	}

	return 0;
}