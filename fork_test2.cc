#if 0
g++ ./fork_test2.cc -o fork_test2 && ./fork_test2
exit $?
#endif

#include <unistd.h>
#include <stdio.h>
#include <iostream>

int main() {
	pid_t fpid; // fpid means the return value of 
	printf("fork!");
	// printf("fork!\n");
	// std::cout << "fork\n";
	fpid = fork();
	if (fpid < 0)
		printf("error in fork!\n");
	else if (fpid == 0)
		printf("I'm the child process, my process id = %d\n", getpid());
	else 
		printf("I'm the parent process, my process id = %d\n", getpid());
	return 0;
}