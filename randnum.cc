#if 0
cc ./randnum.cc -o randnum && ./randnum
exit $?
#endif

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#define N 100000
#define INVALID_NUM 10
#define ARROW_NUM 11

int a[N];

void gen_random(int upper_bound) {
	int i;
	for (i = 0; i < N; i++) {
		a[i] = rand() % upper_bound;
	}
}

/*
int howmany(int value) {
	int count = 0, i;
	for (i = 0; i < N; i++) {
		if (a[i] == value)
			++ count;
	}
	return count;
}
*/

void print_char(int num) {
	if (num <= 9 && num >= 0) {
		printf("%d ", num);
	}
	else if (num == INVALID_NUM) {
		printf("  ");
	}
	else if (num == ARROW_NUM) {
		printf("| ");
	}
	else{
		printf("--");
	}
}

int main() {
	int i, histogram[10] = {0};
	int temp[10];
	int big_count = 0; // the max times that appeared
	srand(time(NULL));
	gen_random(10);
	printf("value\thowmany\n");
	for (i = 0; i < N; i++) {
		histogram[a[i]]++;
	}
	for (i = 0; i < 10; i++) {
		printf("%4d\t%d\n", i, histogram[i]);
		temp[i] = histogram[i];
		big_count = temp[i] > big_count ? temp[i] : big_count;
	}
	printf("-------BELOW---------\n");
	// 1. print the menu num
	for (i = 0; i < 10; i++) {
		print_char(i);
	}
	printf("\n");
	int line_count_max = 1;
	while (big_count % (int)(pow(10, line_count_max)) < big_count) {
		line_count_max++;
	}
	char *map = (char *)malloc(sizeof(char) * line_count_max * 10);
	// memset(map, INVALID_NUM, line_count_max * 10);
	char *middle = (char *)malloc(sizeof(char) * line_count_max);
	for (i = 0; i < 10; i++) {
		// memset(middle, INVALID_NUM, line_count_max * sizeof(char));
		char index = 0;
		while (temp[i] >= 10) {
			middle[index] = temp[i] % 10;
			temp[i] = int(temp[i] / 10);
			index ++;
		}
		middle[index] = temp[i];
		char findex = 0;
		while(index >= 0) {
			map[i * 10 + findex++] = middle[index--];
		}
		while(findex < line_count_max) {
			map[i * 10 + findex++] = INVALID_NUM;
		}
		print_char(ARROW_NUM);
	}
	free(middle);
	printf("\n");
	for (i = 0; i < line_count_max; i++) {
		for (int j = 0; j < 10; j++) {
			print_char(map[j * 10 + i]);
		}
		printf("\n");
	}
	free(map);
	return 0;
}// main