#if 0
g++ ./TemplateDemo01.cc -o TemplateDemo01 && TemplateDemo01
#endif

#include <iostream>

template<typename T>
const T& max(const T& a, const T& b) {
	return a > b ? a : b;
}

template<typename T> class CompareDemo {
	public:
		int compare (const T&, const T&);
		int compare2 (T&, T&);
};

template<typename T>
int CompareDemo<T>::compare(const T& a, const T& b) {
	if ((a - b) > 0)
		return 1;
	else if ((a -b) < 0)
		return -1;
	else
		return 0;
}

template<typename T>
int CompareDemo<T>::compare2(T& a, T& b) {
	if ((a - b) > 0)
		return 1;
	else if ((a -b) < 0)
		return -1;
	else
		return 0;
}

int main() {
	CompareDemo<double> cd;
	std::cout << cd.compare(3.3, 3) << std::endl;
	double a = 2, b = 3;
	std::cout << cd.compare2(a, b) << std::endl;
	std::cout<<max(2.1,2.2)<<std::endl;//模板实参被隐式推演成double
	std::cout<<max<double>(2.1,2.2)<<std::endl;//显示指定模板参数。
	std::cout<<max<int>(2.1,2.2)<<std::endl;//显示指定的模板参数，会将函数函数直接转换为int。
	return 0;
}