#if 0
g++ ./regex_plus.cc -o regex_plus && ./regex_plus
#endif

#include <regex>
#include <iostream>
#include <string>

void show_ip_parts(const std::string& ip) {
	// regular expression with 4 capture groups defined with
	// parenthesis (...)
	const std::regex pattern("(\\d{1,3}):(\\d{1,3}):(\\d{1,3}):(\\d{1,3})");
	// object that will contain the sequence of sub-matches
	std::match_results<std::string::const_iterator> result;
	// match the IP address with the regular expression
	bool valid = std::regex_match(ip, result, pattern);
	std::cout << ip << " \t: " << (valid ? "valid" : "invlaid") << std::endl;
	// if the iP address matched the regex, the print the parts
	if (valid) {
		std::cout << "b1: " << result[1] << std::endl;
		std::cout << "b2: " << result[2] << std::endl;
		std::cout << "b3: " << result[3] << std::endl;
		std::cout << "b4: " << result[4] << std::endl;
	}
}

void show_code_content() {
	std::string base_string("<(/code/:/22/, /detail/:/type 1245265duration 0.060/)>");
	const std::regex pattern("<\\(/code/:/(\\d{1,3})/, /detail/:/(.*)/\\)>");
	std::match_results<std::string::const_iterator> result;
	// match the IP address with the regular expression
	bool valid = std::regex_match(base_string, result, pattern);
	if (valid) {
		std::cout << "code pattern found" << std::endl;
		std::cout << result[1] << std::endl;
		std::cout << result[2] << std::endl;
	}
	else {
		std::cout << "code pattern not found" << std::endl;
	}
}

int main() {
	show_ip_parts("1:22:33:123");
	show_ip_parts("1:22:33:444");
	show_ip_parts("1:22:33:4444");
	show_ip_parts("100:200");

	show_code_content();

	return 0;
}