#if 0

#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define N 18

int origin[N];
int count[N] = {0};

void gen_rand_num(int base) {
	for (int i = 0; i < N; i ++) {
		origin[i] = rand() % base;
	}
}

void print_char() {
	printf(" *");
}

void print_space() {
	printf("  ");
}

int main(int argc, char *argv[]) {
	srand(time(NULL));
	gen_rand_num(10);
	int i;
	for (i = 0; i < N; i ++ ) {
		count[origin[i]] ++;
	}
	// print the menu
	for (i = 0; i < 10; i ++) {
		printf("%2d", i);
	}
	printf("\n");
	bool shouldPrintLine = true;
	while (shouldPrintLine) {
		shouldPrintLine = false;
		for (i = 0; i < 10; i ++) {
			if (count[i] > 0) {
				count[i] --;
				print_char();
			}
			else {
				print_space();
			}
			if (shouldPrintLine == false && count[i] > 0) {
				shouldPrintLine = true;
			}
		}
		printf("\n");
	}
	return 0;
}