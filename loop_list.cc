#if 0
g++ ./loop_list.cc -o loop_list && ./loop_list
exit $?
#endif

#include <iostream>
#include <stdlib.h>
#include <time.h>

typedef struct A {
	int data;
	struct A * next;
} Node;

Node * prepareList(int count) {
	int i;
	Node * header = (Node *)malloc(sizeof(Node)), * temp;
	temp = header;
	temp->data = rand() % 20;
	for (i = 1; i < count; i ++) {
		temp->next = (Node *)malloc(sizeof(Node));
		temp = temp->next;
		temp->data = rand() % 20;
	}
	temp->next = NULL;
	return header;
}

Node * tailOfList(Node * header) {
	Node * tail = header;
	while(tail->next) {
		tail = tail->next;
	}
	return tail;
}

Node * itemOfListAtIndex(Node * header, int index) {
	int count = 0;
	Node * item = header;
	while (count < index && item->next) {
		item = item->next;
		count ++;
	}
	if (count == index) {
		return item;
	}
	else {
		// the index is beyond the range of header list
		return NULL;
	}
}

Node * generateLoopList() {
	int count = rand() % 10 + 8;
	Node * header = prepareList(count);
	int index = random() % 8;
	Node * tail = tailOfList(header);
	Node * item = itemOfListAtIndex(header, index);
	tail->next = item;
	return header;
}

bool isLoopList(Node * header) {
	Node * slowNode = header;
	Node * fastNode = header;
	while (fastNode && fastNode->next) {
		fastNode = fastNode->next->next;
		slowNode = slowNode->next;
		if (fastNode == slowNode) {
			break;
		}
	}
	if (fastNode == slowNode) {
		return true;
	}
	else {
		return false;
	}
}

Node * loopEntranceNode(Node * header) {
	Node * slowNode = header;
	Node * fastNode = header;
	while (fastNode && fastNode->next) {
		fastNode = fastNode->next->next;
		slowNode = slowNode->next;
		if (fastNode == slowNode) {
			break;
		}
	}
	if (fastNode != slowNode) {
		return NULL;
	}

	slowNode = header;
	while(slowNode != fastNode) {
		slowNode = slowNode->next;
		fastNode = fastNode->next;
	}
	return slowNode;
}

void showNodeLIst(Node * header) {
	Node * item = header;
	while(item) {
		printf("%d ", item->data);
		item = item->next;
	}
	printf("\n");
}

int main() {
	srand(time(NULL));
	Node * header = prepareList(10);
	printf("is loop list = %d\n", isLoopList(header));
	showNodeLIst(header);
	header = generateLoopList();
	printf("is loop list = %d\n", isLoopList(header));
	Node * port = loopEntranceNode(header);
	printf("loop entrace = %d\n", port->data);
	return 0;
}