#if 0
g++ ./override_test.cc -o override_test && ./override_test
exit $?
#endif

#include <iostream>

class Base {
public:
	virtual Base *clone() { std::cout << "Base::clone() \n"; return new Base;}
	virtual void copy(Base&) { std::cout << "Base::copy() \n"; }
};

class Derived : public Base {
public:
	Derived *clone() { std::cout << "Derived::clone() \n"; return new Derived;}
	// void copy(Base&) { std::cout << "Derived::copy() \n"; }
	void copy(Derived&) { std::cout << "Derived::copy() \n"; }
};

int main(int arg, char **args) {
	Derived *pd = new Derived;
	pd->clone();

	Base *pb = pd;
	pb->clone();

	//
	// go to test copy
	Derived d;
	Base b;

	pd->copy(d);
	pb->copy(b);

	char *buffer = (char *)malloc(25);
	int type = 36;
	double duration = 4.56789;
	sprintf(buffer, "type=%uduration=%.3f", type, duration);
	std::cout << buffer << std::endl;
	free(buffer);
	return 0;
}