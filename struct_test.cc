#if 0
g++ ./struct_test.cc -o struct_test -lpthread && ./struct_test
exit $?
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

struct A {
	int count;
	int number;
	double value;
};

struct B : A
{
	char age;
};

int main() {
	A a = {10, 22, 9.29};
	printf("A = {%d, %d, %.2f};\n", a.count, a.number, a.value);
	B b;
	b.age = 4;
	printf("Dudu's age = %d;\n", b.age);
	b.count = 22;
	std::cout<<"Orange's birthday = "<<b.count<<std::endl;
	return 0;
}