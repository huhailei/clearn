#if 0
g++ ./regex_test.cc -o regex_test && ./regex_test
exit $?
#endif

#include <regex.h>
#include <iostream>
#include <sys/types.h>
#include <stdio.h>
#include <cstring>
#include <sys/time.h>
#include <string.h>

using namespace std;
const int times = 1;

static char* substr(const char*str, unsigned start, unsigned end)
{
  unsigned n = end - start;
  static char stbuf[256];
  strncpy(stbuf, str + start, n);
  stbuf[n] = 0;
  return stbuf;
}

int main(int argc, char ** argv) {
	//char pattern[512] = "finance\.sina\.cn|stock1\.sina\.cn|3g\.sina\.com\.cn.*(channel=finance|_finance$|ch=stock|/stock/)|dp.sina.cn/.*ch=9&";
    // char pattern[128] = "/code/:/[0-9]*/, /detail/:/[a-z 0-9\.]*/";
    char pattern[64] = "(/code/:/([0-9]{1,8})/, /detail/:/([a-z A-Z0-9\\.]*)/)";
	const size_t nmatch = 10;
	regmatch_t pm[10];
	int z;
	regex_t reg;
	char lbuf[256] = "set", rbuf[256];
	// char buf[3][256] = {"finance.sina.cn/google.com/baidu.com.google.sina.cndddddddddddddddddddddda.sdfasdfeoasdfnahsfonadsdf",
 //                    "3g.com.sina.cn.google.com.dddddddddddddddddddddddddddddddddddddddddddddddddddddbaidu.com.sina.egooooooooo",
 //                    "http://3g.sina.com.cn/google.baiduchannel=financegogo.sjdfaposif;lasdjf.asdofjas;dfjaiel.sdfaosidfj"};
    char buf[3][256] = {"<>","<(/code/:/22/, /detail/:/dudu is a good boy/), (/code/:/22/, /detail/:/orange is a good boy/)>", "<(/code/:/22/, /detail/:/type 1245265duration 0.060/)>"};
    printf("input strings:\n");
    timeval end, start;
    gettimeofday(&start, NULL);
    regcomp(&reg, pattern, REG_EXTENDED);
    // regcomp(&reg, pattern, 0);
    for (int i = 0; i < times; ++i) {
    	for (int j = 0; j < 3; ++j) {
    		z = regexec(&reg, buf[j], nmatch, pm, 0);
    		
    		if (z == REG_NOMATCH) {
    			printf("no match\n");
                continue;
            }
    		else
    			printf("ok\n");
    			
            for (int x = 0; x < nmatch && pm[x].rm_so != -1; ++ x) {
                // if (!x) printf("%04d: %s\n", lno, lbuf);
                printf(" ( %d , %d) ", pm[x].rm_so, pm[x].rm_eo);
                printf("  $%d='%s'\n", x, substr(buf[j], pm[x].rm_so, pm[x].rm_eo));
            }
    	}
    }
    gettimeofday(&end, NULL);
    uint time = (end.tv_sec - start.tv_sec) * 1000000 + end.tv_usec - start.tv_usec;
    cout << time / 1000000 << " s and " << time % 1000000 << " us." << endl;
    return 0;
}