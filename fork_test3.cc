#if 0
cc ./fork_test3.cc -o fork_test3 && ./fork_test3
exit $?
#endif

#include <unistd.h>
#include <stdio.h>

int main() {
	fork();
	(fork() && fork()) || fork(); // this line will just generate 5 process
	fork();
	printf("-");
	return 0;
}