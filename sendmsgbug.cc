#if 0
g++ -o sendmsgbug sendmsgbug.cc -lpthread && ./sendmsgbug
exit $?
#endif

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <assert.h>
#include <utility>
#include <unistd.h>
#include <iostream>

using namespace std;

#define RETRY(X) ({                 \
  int _;                        \
  do { _ = (X); } while(_ == -1 && errno == EINTR); \
  if (_ == -1) perror(#X);              \
  _;                            \
})

struct controlmsg {
  cmsghdr hdr;
  int fd;
};

class FD {
public:
  void SendFD(FD fd) {
    char dummy = 'f';
    iovec iov;
    iov.iov_base = &dummy;
    iov.iov_len = 1;

    controlmsg cmsg;
    memset(&cmsg, 0, sizeof(cmsg));
    cmsg.hdr.cmsg_len = sizeof(cmsg);
    cmsg.hdr.cmsg_level = SOL_SOCKET;
    cmsg.hdr.cmsg_type = SCM_RIGHTS;
    cmsg.fd = fd.fd_;

    msghdr msg;
    memset(&msg, 0, sizeof(msg));
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = &cmsg;
    msg.msg_controllen = sizeof(cmsg);

    assert(RETRY(sendmsg(fd_, &msg, 0)) > 0);
    fd.Close();
  }

  FD GetFD() {
    char dummy = '0';
    iovec iov;
    iov.iov_base = &dummy;
    iov.iov_len = 1;

    controlmsg cmsg;
    memset(&cmsg, 0, sizeof(cmsg));
    cmsg.fd = -1;

    msghdr msg;
    memset(&msg, 0, sizeof(msg));
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
    msg.msg_control = &cmsg;
    msg.msg_controllen = sizeof(cmsg);

    assert(RETRY(recvmsg(fd_, &msg, 0)) > 0);
    assert(dummy == 'f');
    assert(cmsg.fd != -1);
    FD tmp;
    tmp.fd_ = cmsg.fd;
    return tmp;
  }

  void Putc(char c) {
    assert(RETRY(write(fd_, &c, 1)) == 1);
  }

  int Getc() {
  	cout << "test Getc";
    char ret;
    assert(RETRY(read(fd_, &ret, 1)) == 1);  // FAILS HERE, read returns zero because socket is closed prematurely
    return ret;
  }

  void Close() {
    assert(RETRY(close(fd_)) == 0);
    fd_ = -1;
  }

  static pair<FD, FD> mksocketpair() {
    int pipe_fds[2];
    assert(RETRY(socketpair(AF_UNIX, SOCK_STREAM, 0, pipe_fds)) == 0);
    pair<FD, FD> ret;
    ret.first.fd_ = pipe_fds[0];
    ret.second.fd_ = pipe_fds[1];
    return ret;
  }

private:
  int fd_;
};

pair<FD, FD> pipe_1;
pair<FD, FD> pipe_2;

void parent() {
  // Pass file descriptors from pipe_1 to pipe_2
  while (1) pipe_2.first.SendFD(pipe_1.second.GetFD());
}

void child1() {
  // Create file descriptors
  while (1) {
    pair<FD, FD> tmp = FD::mksocketpair();
    pipe_1.first.SendFD(tmp.second);
    tmp.first.Putc('!');
    tmp.first.Close();
  }
}

void child2() {
  // Devour file descriptors
  while (1) {
    FD f = pipe_2.second.GetFD();
    assert(f.Getc() == '!');
    f.Close();
  }
}

int main() {
  cout<<"goto create pipe_1";
  pipe_1 = FD::mksocketpair();
  cout<<"pipe_1 created";
  pipe_2 = FD::mksocketpair();
  if (fork()) child1();
  if (fork()) child2();
  parent();
}
