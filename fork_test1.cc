#if 0
g++ -o fork_test1 fork_test1.cc -lpthread && ./fork_test1
exit $?
#endif

#include <unistd.h>
#include <stdio.h>
int main(void)
{
	int i = 0;
	printf("i\tson/pa\tppid\tpid \tfpid\n");
	// ppid means current process's parent pid
	// pid means current pid
	// fpid means the return value of fork
	for (i = 0; i < 2; i++) {
		pid_t fpid = fork();
		if (fpid == 0) {
			printf("%d\tchild\t%4d\t%4d\t%4d\n", i, getppid(), getpid(), fpid);
		}
		else {
			printf("%d\tparent\t%4d\t%4d\t%4d\n", i, getppid(), getpid(), fpid);
		}
	}
	printf("\n");
	return 0;
}