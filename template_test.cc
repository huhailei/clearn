#if 0
g++ ./template_test.cc -o template_test && ./template_test
#endif

#ifndef TEMPLATE_TEST_H
#define TEMPLATE_TEST_H

#include <iostream>
#include <unistd.h>
#include <stdlib.h>

template<class T> class A {
	public:
		T g(T a, T b) {
			return a + b;
		};
		A(){};
};

int main() {
	A<int> a;
	std::cout<<"2 + 3 = "<<a.g(2, 3)<<std::endl;
	A<double> b;
	std::cout<<"1.8 + 2.4 = "<<b.g(1.8, 2.4)<<std::endl;
	return 0;
}

#endif