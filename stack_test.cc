#include <iostream>
#include <stack>

namespace Orange {
	void StackExample1() {
		std::stack<int> foo1;
		std::cout << "foo1.empty(): " << std::boolalpha << foo1.empty() 
			<< std::endl;

		// push element one by one
		std::cout << "Push four elements: " << std::endl;
		foo1.push(1);
		std::cout << foo1.top() << " ";
		foo1.push(2);
		std::cout << foo1.top() << " ";
		foo1.push(3);
		std::cout << foo1.top() << " ";
		foo1.push(4);
		std::cout << foo1.top() << " " << std::endl;

		std::cout << "foo1.empty(): " << std::boolalpha << foo1.empty() 
			<< std:: endl;
		std::cout << "foo1.size() " << foo1.size() << std::endl;

		std::cout << "弹出四个元素：" << std::endl;
    	std::cout << foo1.top() << " ";
    	foo1.pop();
    	std::cout << foo1.top() << " ";
    	foo1.pop();
    	std::cout << foo1.top() << " ";
    	foo1.pop();
    	std::cout << foo1.top() << " " << std::endl;
    	foo1.pop();
 
    	std::cout << "foo1.empty()：" << std::boolalpha 
        	<< foo1.empty() << std::endl;
    	std::cout << "foo1.size()："
        	<< foo1.size() << std::endl;
	}
}

int main() {
	Orange::StackExample1();
	return 0;
}